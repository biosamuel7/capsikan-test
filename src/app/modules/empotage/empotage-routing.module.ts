import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEmpotageComponent } from './components/create-empotage/create-empotage.component';
import { ListEmpotageComponent } from './components/list-empotage/list-empotage.component';
import { EmpotageComponent } from './empotage.component';

const routes: Routes = [{
  path: '', component: EmpotageComponent,
  children: [
    {
      path: '', 
      redirectTo: 'list'
    },
    {
      path: 'list', 
      component: ListEmpotageComponent,
    },
    {
      path: 'create',
      component: CreateEmpotageComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpotageRoutingModule { }
