import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpotageRoutingModule } from './empotage-routing.module';
import { EmpotageComponent } from './empotage.component';
import { CreateEmpotageComponent } from './components/create-empotage/create-empotage.component';
import { ListEmpotageComponent } from './components/list-empotage/list-empotage.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Step4Component } from './components/step4/step4.component';
import { Step2Component } from './components/step2/step2.component';
import { Step3Component } from './components/step3/step3.component';
import { Step1Component } from './components/step1/step1.component';


@NgModule({
  declarations: [
    EmpotageComponent,
    CreateEmpotageComponent,
    ListEmpotageComponent,
    Step4Component,
    Step2Component,
    Step3Component,
    Step1Component
  ],
  imports: [
    CommonModule,
    EmpotageRoutingModule,
    SharedModule
  ]
})
export class EmpotageModule { }
