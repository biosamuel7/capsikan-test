import { StepModel } from "../models/step.model";

export const StepData: StepModel[] = [
    {
        title: 'Prérequis',
        text: 'Lorem ipsum dolor sit amet, consetetur'
    },
    {
        title: 'Site',
        text: 'Lorem ipsum dolor sit amet, consetetur'
    },
    {
        title: 'Lots',
        text: 'Lorem ipsum dolor sit amet, consetetur'
    },
    {
        title: 'Recap',
        text: 'Lorem ipsum dolor sit amet, consetetur'
    }
]