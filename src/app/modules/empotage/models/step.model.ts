export interface StepModel {
    title: string;
    text: string;
}