import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEmpotageComponent } from './list-empotage.component';

describe('ListEmpotageComponent', () => {
  let component: ListEmpotageComponent;
  let fixture: ComponentFixture<ListEmpotageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListEmpotageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEmpotageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
