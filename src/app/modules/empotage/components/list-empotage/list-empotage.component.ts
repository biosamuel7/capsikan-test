import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-empotage',
  templateUrl: './list-empotage.component.html',
  styles: [
  ]
})
export class ListEmpotageComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goCreateEmpotage() {
    this.router.navigate(['/empotage/create'])
  }
}
 