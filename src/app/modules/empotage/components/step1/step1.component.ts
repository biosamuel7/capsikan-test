import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styles: [
  ]
})
export class Step1Component implements OnInit {

  @Output()
  goStep: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
