import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmpotageComponent } from './create-empotage.component';

describe('CreateEmpotageComponent', () => {
  let component: CreateEmpotageComponent;
  let fixture: ComponentFixture<CreateEmpotageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEmpotageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmpotageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
