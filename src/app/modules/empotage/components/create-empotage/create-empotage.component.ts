import { Component, OnInit } from '@angular/core';
import { StepData } from '../../mocks/step.mock';

@Component({
  selector: 'app-create-empotage',
  templateUrl: './create-empotage.component.html',
  styles: [
  ]
})
export class CreateEmpotageComponent implements OnInit {

  stepActived: number = 0;

  stepData = StepData;

  constructor() { }

  ngOnInit(): void {
  }

}
