import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styles: [
  ]
})
export class Step2Component implements OnInit {

  @Output()
  goStep: EventEmitter<number> = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

}
