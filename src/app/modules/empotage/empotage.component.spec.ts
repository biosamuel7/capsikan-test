import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpotageComponent } from './empotage.component';

describe('EmpotageComponent', () => {
  let component: EmpotageComponent;
  let fixture: ComponentFixture<EmpotageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpotageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpotageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
