import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WrapperRoutingModule } from './wrapper-routing.module';
import { WrapperComponent } from './wrapper.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    WrapperComponent,
    SidebarComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    WrapperRoutingModule,
    SharedModule
  ]
})
export class WrapperModule { }
