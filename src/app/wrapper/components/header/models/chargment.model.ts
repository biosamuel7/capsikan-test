export interface ChargmentModel {
    icon: string;
    label: string;
    isActive: boolean;
}