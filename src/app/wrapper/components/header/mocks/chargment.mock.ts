import { ChargmentModel } from "../models/chargment.model";

export const chargmentData: ChargmentModel[] = [
    {
        icon: 'assets/icons/noun_Cashew_2776296.svg',
        label: 'Cajou',
        isActive: true
    },
    {
        icon: 'assets/icons/noun_cacao_3890993.svg',
        label: 'Cacao',
        isActive: false
    },
    {
        icon: 'assets/icons/noun_Rubber_3859852.svg',
        label: 'Hévéa',
        isActive: false
    },
    {
        icon: 'assets/icons/shea-butter.svg',
        label: 'Karité',
        isActive: false
    }
]