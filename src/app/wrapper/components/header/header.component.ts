import { Component, OnInit } from '@angular/core';
import { chargmentData } from './mocks/chargment.mock';
import { ChargmentModel } from './models/chargment.model';

@Component({
  selector: 'wrapper-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  chargmentData: ChargmentModel[] = chargmentData;

  constructor() { }

  ngOnInit(): void {

  }

}
