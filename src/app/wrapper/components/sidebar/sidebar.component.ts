import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wrapper-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {

  navigations = [
    {
      path: '/dashboard',
      icon: 'assets/icons/dashboard.svg',
      label: 'Tableau de bord'
    },
    {
      path: '/empotage',
      icon: 'assets/icons/gear.svg',
      label: 'Opérations'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
