import { WrapperComponent } from './wrapper.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: '', component: WrapperComponent,
  children: [
    {
      path: '', 
      redirectTo: 'empotage',
    },
    {
      path: 'empotage',
      loadChildren: async () => (await import('../modules/empotage/empotage.module')).EmpotageModule
    },
    {
      path: 'dashboard',
      loadChildren: async () => (await import('../modules/dashboard/dashboard.module')).DashboardModule
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WrapperRoutingModule { }
